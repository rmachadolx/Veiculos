﻿








namespace Veiculos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBoxMatricula = new System.Windows.Forms.TextBox();
            this.TextBoxDeposito = new System.Windows.Forms.TextBox();
            this.TextBoxConsumo = new System.Windows.Forms.TextBox();
            this.ButtonNovoCarro = new System.Windows.Forms.Button();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.TextBoxLitros = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LabelPercorrer = new System.Windows.Forms.Label();
            this.LabelAbastecer = new System.Windows.Forms.Label();
            this.ButtonNovaViagem = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBoxDistancia = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Matricula:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(38, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Depósito:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(38, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Consumo:";
            // 
            // TextBoxMatricula
            // 
            this.TextBoxMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxMatricula.Location = new System.Drawing.Point(148, 31);
            this.TextBoxMatricula.Name = "TextBoxMatricula";
            this.TextBoxMatricula.Size = new System.Drawing.Size(100, 22);
            this.TextBoxMatricula.TabIndex = 4;
            // 
            // TextBoxDeposito
            // 
            this.TextBoxDeposito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxDeposito.Location = new System.Drawing.Point(148, 80);
            this.TextBoxDeposito.Name = "TextBoxDeposito";
            this.TextBoxDeposito.Size = new System.Drawing.Size(100, 22);
            this.TextBoxDeposito.TabIndex = 6;
            // 
            // TextBoxConsumo
            // 
            this.TextBoxConsumo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxConsumo.Location = new System.Drawing.Point(148, 117);
            this.TextBoxConsumo.Name = "TextBoxConsumo";
            this.TextBoxConsumo.Size = new System.Drawing.Size(100, 22);
            this.TextBoxConsumo.TabIndex = 7;
            // 
            // ButtonNovoCarro
            // 
            this.ButtonNovoCarro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonNovoCarro.Location = new System.Drawing.Point(157, 217);
            this.ButtonNovoCarro.Name = "ButtonNovoCarro";
            this.ButtonNovoCarro.Size = new System.Drawing.Size(75, 50);
            this.ButtonNovoCarro.TabIndex = 8;
            this.ButtonNovoCarro.Text = "Novo Veiculo";
            this.ButtonNovoCarro.UseVisualStyleBackColor = true;
            this.ButtonNovoCarro.Click += new System.EventHandler(this.ButtonNovoCarro_Click);
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Location = new System.Drawing.Point(38, 297);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(10, 13);
            this.LabelStatus.TabIndex = 9;
            this.LabelStatus.Text = " ";
            // 
            // TextBoxLitros
            // 
            this.TextBoxLitros.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxLitros.Location = new System.Drawing.Point(148, 154);
            this.TextBoxLitros.Name = "TextBoxLitros";
            this.TextBoxLitros.Size = new System.Drawing.Size(100, 22);
            this.TextBoxLitros.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 16);
            this.label2.TabIndex = 10;
            this.label2.Text = "Litros:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Pode Percorrer:";
            // 
            // LabelPercorrer
            // 
            this.LabelPercorrer.AutoSize = true;
            this.LabelPercorrer.Location = new System.Drawing.Point(125, 185);
            this.LabelPercorrer.Name = "LabelPercorrer";
            this.LabelPercorrer.Size = new System.Drawing.Size(0, 13);
            this.LabelPercorrer.TabIndex = 13;
            // 
            // LabelAbastecer
            // 
            this.LabelAbastecer.AutoSize = true;
            this.LabelAbastecer.Location = new System.Drawing.Point(265, 157);
            this.LabelAbastecer.Name = "LabelAbastecer";
            this.LabelAbastecer.Size = new System.Drawing.Size(0, 13);
            this.LabelAbastecer.TabIndex = 15;
            // 
            // ButtonNovaViagem
            // 
            this.ButtonNovaViagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonNovaViagem.Location = new System.Drawing.Point(392, 217);
            this.ButtonNovaViagem.Name = "ButtonNovaViagem";
            this.ButtonNovaViagem.Size = new System.Drawing.Size(75, 50);
            this.ButtonNovaViagem.TabIndex = 16;
            this.ButtonNovaViagem.Text = "Nova Viagem";
            this.ButtonNovaViagem.UseVisualStyleBackColor = true;
            this.ButtonNovaViagem.Click += new System.EventHandler(this.ButtonNovaViagem_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(302, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 16);
            this.label6.TabIndex = 17;
            this.label6.Text = "Distancia:";
            // 
            // TextBoxDistancia
            // 
            this.TextBoxDistancia.Location = new System.Drawing.Point(392, 32);
            this.TextBoxDistancia.Name = "TextBoxDistancia";
            this.TextBoxDistancia.Size = new System.Drawing.Size(100, 20);
            this.TextBoxDistancia.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 461);
            this.Controls.Add(this.TextBoxDistancia);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ButtonNovaViagem);
            this.Controls.Add(this.LabelAbastecer);
            this.Controls.Add(this.LabelPercorrer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TextBoxLitros);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.ButtonNovoCarro);
            this.Controls.Add(this.TextBoxConsumo);
            this.Controls.Add(this.TextBoxDeposito);
            this.Controls.Add(this.TextBoxMatricula);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
      //  private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBoxMatricula;
   //     private System.Windows.Forms.TextBox TextBoxKm;
        private System.Windows.Forms.TextBox TextBoxDeposito;
        private System.Windows.Forms.TextBox TextBoxConsumo;
        private System.Windows.Forms.Button ButtonNovoCarro;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.TextBox TextBoxLitros;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LabelPercorrer;
        private System.Windows.Forms.Label LabelAbastecer;
        private System.Windows.Forms.Button ButtonNovaViagem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBoxDistancia;
    }
}

