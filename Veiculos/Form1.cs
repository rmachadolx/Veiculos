﻿namespace Veiculos
{
    using System;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;


    public partial class Form1 : Form
    {
        private Veiculo veiculo;


        public Form1()
        {
            InitializeComponent();
            ButtonNovaViagem.Enabled = false;
            TextBoxDistancia.Enabled = false;
        }

        private void ButtonNovoCarro_Click(object sender, EventArgs e)
        {
            Regex regexMatricula = new Regex("^[0-9]{2}-[a-zA-Z]{2}-[0-9]{2}$");
            Match matchMatricula = regexMatricula.Match(TextBoxMatricula.Text);

            double capacidade;
            double consumo;
            double litros;

            if (!matchMatricula.Success)
            {
                MessageBox.Show("Introduza a Matricula no formato correcto!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                TextBoxMatricula.Focus();

            }
            else if (!double.TryParse(TextBoxDeposito.Text, out capacidade))
            {
                MessageBox.Show("Introdua a capacidade do deposito em litros!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TextBoxDeposito.Focus();
            }
            else if (!double.TryParse(TextBoxConsumo.Text, out consumo))
            {
                MessageBox.Show("Introdua consumo médio em litros por 100km!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TextBoxConsumo.Focus();
            }
            else if (!double.TryParse(TextBoxLitros.Text, out litros))
            {
                MessageBox.Show("Introdua os litros que pretnde abastecer!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TextBoxLitros.Focus();
            }
            else
            {
                veiculo = new Veiculo(0, Convert.ToDouble(TextBoxDeposito.Text), Convert.ToInt32(TextBoxLitros.Text), Convert.ToDouble(TextBoxConsumo.Text), 0, TextBoxMatricula.Text);

                if (veiculo.MeteCombustivel(Convert.ToDouble(TextBoxLitros.Text)) >= 0)
                {
                    
                    LabelPercorrer.Text = veiculo.Percorrer().ToString();
                    LabelStatus.Text = veiculo.ToString();
                    ButtonNovaViagem.Enabled = true;
                    TextBoxDistancia.Enabled = true;
                    ButtonNovoCarro.Enabled = false;
                    TextBoxMatricula.Enabled = false;
                    TextBoxDeposito.Enabled = false;
                    TextBoxConsumo.Enabled = false;
                    TextBoxLitros.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Introdua os litros que pretnde abastecer inferior ou igual à capacidade do depósito!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    TextBoxLitros.Focus();
                }

            }








        }

        private void ButtonNovaViagem_Click(object sender, EventArgs e)
        {
            int distancia;
            if (!Int32.TryParse(TextBoxDistancia.Text, out distancia))
            {
                MessageBox.Show("Introdua a em Kilometrot!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TextBoxDistancia.Focus();
            }
            else
            {
                veiculo.RegistaViagem(Convert.ToDouble(TextBoxDistancia.Text));
                LabelStatus.Text = veiculo.ToString();
            }

        }
    }
}
